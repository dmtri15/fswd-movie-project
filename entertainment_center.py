from media import Movie
from fresh_tomatoes import open_movies_page
# initialize several movies objects
# title, url, id
tdk = Movie('The Dark Knight',
            'http://static2.comicvine.com/uploads/original/0/4115/885630-1new_joker_poster_for_the_dark_knight.jpg',
            'https://www.youtube.com/watch?v=EXeTwQWrcwY')
wc = Movie('Warcraft',
            'https://upload.wikimedia.org/wikipedia/en/5/56/Warcraft_Teaser_Poster.jpg',
            'https://www.youtube.com/watch?v=RhFMIRuHAL4')
xmen= Movie('Xmen Apocalypse',
            'http://blogs-images.forbes.com/scottmendelson/files/2016/03/8597c6f8d30a096991f53d0e8081feadc1f43ded.jpg',
            'https://www.youtube.com/watch?v=COvnHv42T-A')
# create a list of movies object
movies_list = [tdk, wc, xmen]
# call the open_movies_page method
open_movies_page(movies_list)
