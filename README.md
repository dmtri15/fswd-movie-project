## Synopsis

This is the movie trailer website project from Udacity's Full Stack Web Development nanodegree course. This website will programmatically generate a html page that displays movies along with their titles, posters and youtube trailers

## Installation

Please download the zip file or clone the project at: [project link](https://bitbucket.org/dmtri15/fswd-movie-project)

then `cd` into the project and run the following line in the command line (with Python installed):
>```python entertainment_center.py``` 

## Contributors

If you catch any bug, feel free to open a new pull request or issue

## License

MIT license